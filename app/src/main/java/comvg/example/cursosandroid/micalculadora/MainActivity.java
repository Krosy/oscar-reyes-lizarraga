package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Operaciones ops = new Operaciones();
    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResul;
    private Button btnSuma;
    private Button btnResta;
    private Button btnDivi;
    private Button btnMult;
    private Button btnLimpiar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initComponents();

    }

    public void initComponents()
    {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);
        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnDivi = (Button) findViewById(R.id.btnDivi);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        this.setEventos();
    }

    public void setEventos()
    {
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnSuma:
                ops.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                ops.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(ops.suma()));
                break;
            case R.id.btnResta:
                ops.setNum1(Integer.parseInt(txtNum1.getText().toString()));
                ops.setNum2(Integer.parseInt(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(ops.resta()));
                break;
            case R.id.btnMult:
                ops.setNum1(Integer.parseInt(txtNum1.getText().toString()));
                ops.setNum2(Integer.parseInt(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(ops.mult()));
                break;
            case R.id.btnDivi:
                ops.setNum1(Integer.parseInt(txtNum1.getText().toString()));
                ops.setNum2(Integer.parseInt(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(ops.div()));
                break;
            case R.id.btnCerrar:
                finish();
                break;
            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResul.setText("");
                break;
        }
    }
}
